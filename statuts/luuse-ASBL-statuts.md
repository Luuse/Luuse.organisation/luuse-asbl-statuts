---
title: "Luuse ASBL Statuts (fr)"
date: 2020
geometry: "left=3cm,right=3cm,top=2cm,bottom=3cm"
output: pdf_document
fontsize: 12pt
classoption:
- twelvecolumn
---

---
## Titre I - Dénomination, siège social, but, durée  

### Article 1 : Dénomination

L’association est dénommée «Luuse». Cette dénomination doit figurer dans tous les actes, factures, annonces, publications, lettres, notes de commande et autres documents émanant de l’association, et être immédiatement précédée ou suivie des mots «association sans but lucratif» ou «vereniging zonder winstoogmerk», ou bien de l’abréviation «asbl» / «vzw», avec l’indication précise de l’adresse du siège de l’association.

### Article 2 : Siège social

Son siège social est établi rue Alexandre Markelbach, 82 - 1030 Schaerbeek dans l’arrondissement judiciaire de Bruxelles.Toute modification du siège social est de la compétence exclusive de l’assemblée générale qui votera sur ce point, conformément à la loi du 27 juin 1921, adaptée et modifiée par la loi du 2 mai 2002.

### Article 3 : But, objet social poursuivi, champ d’action

Luuse asbl vise à développer des méthodes d'éditions de forme et de publication alternatives. Elle s'attache à défendre une approche réfléchie, curieuse et consciente des relations entre outils de production, designer et utilisateur·ice·s. Dans cette optique, Luuse asbl œuvre par capillarité entre les champs d'action de la pédagogie, de la recherche et de la commande.

Luuse asbl valorise dans ses activités les systèmes ouverts et la culture libre par le développement d'environnements physiques et numériques, tant pratiques que théoriques.La transmission réciproque des savoirs et constitutions de communs, tant en interne qu'en externe conditionne sa raison d'être.

L'association Luuse s'adresse à des groupes ou individus divers : artistes, artisans, associations, designer, publics, institutions - y compris les écoles, hautes-écoles et universités - théoricien·ne·s, institutions médiatiques...


Pour atteindre ces buts, Luuse asbl se dote de moyens tels que l'organisation ponctuels d'événements (ateliers, exposition, conférence...) dans des cadres institutionnels ou non, d'un lieu de travail et d'échange, de la constitution d'outils spécifiques et de temps consacré à la recherche au travers de travaux auto-initiés oucontractuels. Les membres de Luuse asbl portent ainsi une attention particulière à la documentation des activités de l'association.


### Article 4 : Durée de l’association

L’association est constituée pour une durée indéterminée. Elle peut être dissoute à tout moment

---

## Titre II – Membres

### Article 5 : Membres

L’association est composée de membres effectifs et de membres adhérents. Le nombre demembres effectifs ne peut être inférieur à trois. Ceux-ci si divisent en deux catégories, les membres actifs et les membres observateurs dont leur rôle est défini dans le ROI.


### Article 6 : Rôle d’intendant et rapports

Un rôle d’intendant chargé de faire le lien entre les membres et le conseil d’administration est mis en place.Choisi par les membres effectifs selon un rythme et des critères précisés dans la ROI, l’intendant est chargé d’effectuer un rapport des activités menées par les membres au minimum une fois par semestre. Ce rapport peut prendre la forme d’au moins un courriel ou d’une rencontre avec l’un ou plusieurs des membres du conseil d’administration


### Article 7 : Admission – démission – suspension – exclusion de membres et membres réputés démissionnaires

Tout candidat aux différents statuts de membres, décrits dans la ROI, doit suivre la démarche qui y est décrite.

Tout membre est libre de se retirer de l’association en adressant par courriel sa démission au conseild’administration avec un préavis d'un mois.

Est réputé démissionnaire le membre qui n’assiste pas ou qui ne se fait pas représenter à trois assemblées générales consécutives si sa présence est demandée selon la ROI.

L’exclusion d’un membre ne peut être prononcée que par l’assemblée générale à la majorité des deux tiers des voix présentes ou représentées. Le conseil d’administration peut suspendre, jusqu’à décision de l’assemblée générale, les membres qui se seraient rendus coupables d’infraction grave aux statuts ou aux lois.

L’exclusion d’un membre requiert les conditions suivantes :
* La mention dans l’ordre du jour de l’assemblée générale de la proposition d’exclusion avec la mention, aumoins sommaire, de la raison de cette proposition.
* La convocation régulière d’une assemblée générale où tous les membres effectifs doivent être convoqués.
* La décision de l’assemblée générale doit être prise à la majorité des 2/3 des voix des membres votantsprésents ou représentés.
* Le respect des droits de la défense, c’est-à-dire l’audition du membre dont l’exclusion est demandée, si celui-ci le souhaite.
* La mention dans le registre de l’exclusion du membre.

S’agissant d’une décision concernant une personne, celle-ci devra impérativement être prise par vote secret.La qualité de membre se perd automatiquement par le décès ou, s’il s’agit d’une personne morale, par ladissolution, la fusion, la scission, la nullité ou la faillite.

Le membre démissionnaire, suspendu ou exclu, ainsi que les héritiers ou ayants droit du membre décédé, n’ont aucun droit sur le fonds social de l’association. Ils ne peuvent réclamer ou requérir ni relevé, ni reddition de compte, ni apposition de scellés, ni inventaires, ni le remboursement des cotisations versées s’il y a.

### Article 8 : Registre des membres

L’association doit tenir un registre des membres, sous la responsabilité du conseil d’administration.Toute décision d’admission, de démission ou d’exclusion de membres est inscrite au registre à la diligence du conseil d’administration dans les quinze jours de la connaissance que le conseil a eu des modifications intervenues.

Tous les membres peuvent consulter le registre des membres, ainsi que tous les procès-verbaux et décisions de l’assemblée générale, du conseil d’administration, de même que tous les documents comptables de l’association, sur simple demande par courriel, motivée et adressée à tous les membres du conseil d’administration. Les membres sont tenus de préciser les documents auxquels ils souhaitent avoir accès. Le conseil d’administration est tenu d'envoyer les documents demandés dans le mois qui suit la demande.

---

## Titre III - Cotisations

### Article 9 : Cotisations

Les membres effectifs de Luuse ASBL s'engagent à payer une cotisation dont le montant est défini dans le ROI.

## Titre IV - Assemblée générale

### Article 10 : Composition

L'assemblée générale est composée de tous les membres effectifs. Seules cette catégorie de membres y a voixdélibérative. Les membres adhérents peuvent assister aux assemblées générales, mais n'y ont pas voix délibérative.Le conseil d'administration désigne un de ces membres pour présider chaque réunion de l'assemblée générale.

### Article 11 : Admission

Peuvent devenir membres effectifs toute personne démontrant une pratique active et régulière en lien avec lesbuts de l’association. Ils peuvent ensuite postuler ou être invité par un ou plusieurs des membres effectifs.

Leur entrée en tant que membres effectifs est voté par les membres effectifs au cours de leur réunion ordinaire et approuvée par une majorité des 3/4 des membres effectifs présents ou représentés.

L’admission ou le rejet est définitif et n’ont pas besoin d'être motivés par les membres effectifs. Il est porté à la connaissance du candidat par courriel

### Article 12 : Pouvoirs

L’assemblée générale est le pouvoir souverain de l’association. Elle est notamment compétente pour :
* la modification des statuts;
* l’exclusion de membres;
* la nomination et la révocation des administrateurs, des vérificateurs aux comptes et du ou des liquidateurs;
* la fixation de la rémunération des vérificateurs aux comptes dans les cas où une rémunération est attribuée;
* l’approbation des comptes et des budgets;
* la décharge à octroyer annuellement aux administrateurs, aux vérificateurs volontaire, aux liquidateurs;
* la dissolution volontaire de l’association;
* la transformation éventuelle en société à finalité sociale ;
* la décision de la destination de l’actif en cas de dissolution de l’association;
* tous les cas exigés dans les statuts;
* l’approbation du règlement d’ordre intérieur et ses modifications ;
* décider d’intenter une action en responsabilité contre tout membre de l’association, tout administrateur, tout vérificateur aux comptes (ou commissaire aux comptes), toute personne habilitée à représenter l’association ou tout mandataire désigné par l’assemblée générale.

### Article 13 : Convocation – Assemblée générale ordinaire

Les membres effectifs sont convoqués à l’assemblée générale ordinaire, qui aura lieu aumoins une fois par an.

L’assemblée générale est convoquée par l’administrateur désigné par le conseil d’administration, par courriel au moins quinze jours avant la date de celle-ci. La convocation doit préciser la date, l’heure, le lieu et l’ordre du jour.

### Article 13 .2: Convocation – Assemblée générale extraordinaire

L’assemblée générale doit être convoquée par le conseil d’administration deux des membres effectifs en font la demande écrite. De même, toute proposition soumise par un membres effectif doit être portée à l’ordre du jour de l’assemblée générale suivante.

### Article 14 : Délibération

L’assemblée générale délibère valablement dès que les deux tiers de ses membres ayant un pouvoir de vote sont présents ou représentés, sauf dans le cas où la loi exige un quorum de présences et un quorum de votes, soit la modification statutaire, l’exclusion d’un membre, la dissolution de l’asbl ou la transformation en société à finalité sociale.

L’assemblée ne peut délibérer valablement que sur les points inscrits à l’ordre du jour.Les décisions sont prises à la majorité simple des voix présentes ou représentées, sauf dans le cas où il en est décidé autrement par la loi ou les présents statuts. En cas de partage des voix, le vote est décrété nul, soumis à discussion puis revoté à l’assemblée suivante.

### Article 15 : Représentation

Tout membre peut se faire représenter par un autre membre à qui il donne procuration écrite ou par courriel. Tout membre ne peut détenir qu’une procuration.

### Article 16 : Modifications statutaires et dissolutions

L'assemblée générale ne peut valablement délibérer sur les modifications aux statuts que si les modifications sont explicitement indiquées dans la convocation et si l'assemblée réunit au moins les deux tiers des membres effectifs, qu'ils soient présents ou représentés.

Aucune modification ne peut être adoptée qu'à la majorité des deux tiers des voix des membres présents ou représentés.

Toutefois, la modification qui porte sur le ou les buts en vue desquels l'association est constituée, ne peut êtreadoptée qu'à la majorité des quatre cinquièmes des voix des membres présents ou représentés. Si les deux tiers des membres effectifs ne sont pas présents ou représentés à la première réunion, il peut être convoqué une seconde réunion qui pourra délibérer valablement, quel que soit le nombre des membres présents ou représentés, et adopter les modifications aux majorités prévues à l'alinéa 2 ou à l'alinéa 3. La seconde réunionne peut être tenue moins de quinze jours après la première réunion. Toute modification aux statuts ou décision relative à la dissolution doit être déposée, dans le mois de sa date, au greffe du tribunal de commercepour publication aux Annexes du Moniteur belge. Il en est de même pour toute nomination ou cessation de fonction d’un administrateur, d’une personne habilitée à représenter l’association, d’une personne déléguée à la gestion journalière ou d’un vérificateur aux comptes.


### Article 17 : Publicité des décisions prises par l’assemblée générale

Les convocations et procès-verbaux, dans lesquels sont consignées les décisions de l’assemblée générale, ainsi que tous les documents comptables, sont signés par un administrateur. Ils sont conservés dans un registre et peuvent y être consultés par tous les membres effectifs et par des tiers s’ils en justifient la raison etque celle-ci est acceptée par le conseil d’administration.

---

## Titre V - Conseil d’administration


### Article 18 : Mandat au conseil d'administration

Le conseil d’administration de l’association est composé de 3 membres effectifs au moins et de 5 au plus, nommés et révocables par l’assemblée générale.La durée du mandat est illimité. Leur mandat n’expire que par décès, démission ou révocation.


### Article 19 : Élection au conseil d'administration

Une élection à un poste d'administrateur non attribué a lieu sur proposition du conseil d'administration, ou lorsqu'un tiers des membres effectifs en fait la demande.

Toute personne est éligible au poste d'administrateur de l'asbl. Les administrateurs sortants sont rééligibles.Les candidatures comme administrateur devront parvenir par courriel et être signées par au moins troismembres effectifs après leur rencontre physique, téléphonique, télématique ou par un moyen approprié pour ressentir la tessiture de la voix du candidat, ceci 7 jours au moins avant l’assemblée générale. Le nombre d’administrateurs sera toujours inférieur au nombre de membres effectifs de l’assemblée générale.

L'élection au Conseil d'administration se fait poste par poste.L'élection est acquise à la majorité absolue des suffrages exprimés. Si aucun candidat n'obtient la majoritéabsolue, un tour additionnel de scrutin est organisé entre les deux candidats ayant obtenu le plus de voix au premier tour. Les candidats en situation d'égalité des voix pour la deuxième place se représentent tous au touradditionnel, et on procède à un nouveau tour de vote jusqu'à ce qu'une majorité absolue se dégage

### Article 20 : Démission

Tout administrateur qui veut démissionner doit signifier sa décision par courriel au conseil d’administrationdans un délais de 7 jours précédant sa démission effective.Article 21 : Fréquence des réunionsLe conseil d’administration se réunit dès que les besoins s’en font sentir. Il est convoqué à la demande de trois administrateurs au moins. Cependant, un conseil d'administration devra se tenir au moins une fois par trimestre. Il est présidé par un administrateur désigné en préambule à chaque réunion.

### Article 22 : Délibération

Le Conseil d’administration délibère valablement dès que la moitié de ses membres sont présents oureprésentés. Les décisions du conseil d’administration sont prises à la majorité simple des voix présentes ou représentées.

En cas de partage des voix, et après avoir épuisé les possibilités de tours suivants, le point est reporté auprochain conseil d’administration.

### Article 23 : Pouvoirs

Le conseil d’administration a les pouvoirs les plus étendus pour l’administration et la gestion de l’association. Le conseil d’administration fonctionne sur un mode collégial. Toutes les attributions qui ne sont pas expressément réservées par la loi ou les statuts à l’assemblée générale seront exercées par le conseil d’administration.Il peut notamment, sans que cette énumération soit limitative, faire et passer tous les actes et contrats, ouvrir et gérer tout compte bancaire, transiger, compromettre, acquérir, échanger, vendre tout bien, meuble ou immeuble, hypothéquer, emprunter, conclure des baux, accepter tout leg, subside, donation et transfert, renoncer à tout droit, représenter l’association en justice, tant en défendant qu’en demandant. Il peut aussi nommer et révoquer le personnel de l’association.


### Article 24 : Délégation à la gestion journalière

Le conseil d’administration peut déléguer certains pouvoirs à plusieurs personnes, administrateurs ou non,agissant conjointement.

Les pouvoirs de l’organe de gestion journalière sont limités aux actes de gestion quotidienne de l’association qui permet d’accomplir les actes d’administration :

* qui ne dépassent pas les besoins de la vie quotidienne de l’asbl;
* qui, en raison de leur peu d’importance et de la nécessité d’une prompte solution, ne justifient pasl’intervention du conseil d’administration.

La durée du mandat des délégués à la gestion journalière, éventuellement renouvelable, est fixée par le conseil d’administration. Quand le délégué à la gestion journalière exerce également la fonction d’administrateur, la fin du mandat d’administrateur entraîne automatiquement la fin du mandat du délégué à la gestion journalière.

Le conseil d’administration peut, à tout moment et sans qu’il doive se justifier, mettre fin à la fonction exercée par la personne chargée de la gestion journalière.

### Article 25 : Représentation

L’association peut être valablement représentée dans tous les actes ou en justice par deux administrateurs au moins désignés par le conseil d’administration agissant conjointement qui, en tant qu’organe, ne devront pas justifier vis-à-vis des tiers d’une décision préalable et d’une procuration du conseil d’administration.


